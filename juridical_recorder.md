---
layout: page
title: Juridical Recorder für ETCS und DSTW
permalink: /juridical_recorder/
hide_from_menu: true
---

Im Bereich ETCS/ERTMS existiert eine Spezifikation im Subset-027 zum *Juridical Recording* im Sinne der Unfalldatenspeicherung.
Diese wird von den Herstellern durch eine *Juridical Recording Unit* (JRU) bereitgestellt, die neben den Ereignissen des ETCS-Hauptrechners (European Vital Computer / EVC) auch die Signale der Class-B-Systeme speichert.
Die Fälschungssicherheit des Datenspeichers steht hierbei im Vordergrund.
Neben allen externen Ereignissen, wie z. B. erkannten Eurobalisen, und internen Ereignissen, wie z. B. betätigte Bremsen, wird mindestens alle fünf Sekunden eine allgemeine Zustandsinformation gespeichert (mit Zeitstempel, Zugposition, Geschwindigkeit, Betriebssystemversion, ETCS-Level und -Modus).

Der dritte Anwendungsfall befasst sich mit der Überführung einer heutigen dedizierten, physischen *Juridical Recording Unit* auf eine Blockchain-basierte Lösung mit Echtzeitanforderungen (<1 Sekunde Block-Zyklus-Zeit) zur Erprobung in einem geeigneten Testumfeld.
Neben der Fälschungssicherheit des Datenspeichers wird hier auch auf Interoperabilitätsanforderungen zwischen mehreren Herstellern geachtet (z. B. standardisierte Kommunikationsprotokolle). Ein weiteres wichtiges Ziel des Projektes ist der Nachweis der praktischen Tauglichkeit unter Verwendung von Standard-Hardwarekomponenten.

Ein wesentlicher Erfolg des Projektes ist, dass der Nachweis der Echtzeitanforderung für den Versuchsaufbau im Labor und im Testzug erbracht werden konnte. Die Anzahl der aufzuzeichnenden Signale einer JRU sind mit ca. 70 Werten bestimmt. Der Versuchsaufbau im Zug war in der Lage bis zu 800 Signale innerhalb der geforderten 1s Konsenzzeit zu speichern.
Die Referenzimplementierung ist [hier](../referenzimp/) beschrieben und der [Quelltext](https://github.com/ibr-ds/zugchain) ist quelloffen.
Die Ergebnisse sind außerdem [hier](https://ieeexplore.ieee.org/abstract/document/9833566) veröffentlicht.

Ein weiterer Teil der Betrachtung ist die Verfügbarkeit des verteilten Systems im Vergleich zu den Anforderungen einer bestehenden JRU-Spezifikation. Die am Markt verfügbaren Modelle erreichen die spezifizierten Werte durch hochwertige und spezialisierte Speicherkomponenten und entsprechend crashsicher ausgelegte Hardware. Die Untersuchungen im Projekt gehen von statistischen Erhebungen realer Unfalldaten. Sie haben das Ziel bei wesentlich geringer crashsicheren Einzelkomponenten, durch die verteilte Architektur und die Absicherung des identischen Datenbestandes eine vergleichbare Verfügbarkeit nachzuweisen.

Die Ergebnisse zu diesem Anwendungsfall sind in den Veröffentlichungen ["Zuverlässigkeit von verteiltem *Juridical Recording*"](https://www.eurailpress.de/abo/signal-draht.html) (Signal & Draht, Ausgabe September
2021, [hier nachzulesen]({{ site.url }}/assets/2021-09_Signal-Draht_Zuverlässigkeit-Juridical-Recording_Braband-Schaebe.pdf)) und ["ZugChain: Blockchain-Based Juridical Data Recording in Railway Systems"](https://ieeexplore.ieee.org/abstract/document/9833566) zusammengefasst.
