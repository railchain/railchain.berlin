---
layout: page
title: Ausstellen eines Typenschildes
permalink: /asset_id_Typenschild/
hide_from_menu: true
---


Der Vorgang zum Ausstellen eines Typenschildes ist im folgenden Bild dargestellt:
![Ausstellen eines Typenschildes](../images/Railchain_AssetId_Typenschild_pos.png)

Der Aufbau der ersten Verbindung zwischen einem Asset und seinem Erstbesitzer ist dabei ein kritischer Vorgang. Der Erstbesitzer wird der Hersteller eines Assets sein, daher kann in der Firmware eines Gerätes beim ersten "Erwachen" davon ausgegangen werden, dass sich das Asset in einer abgesicherten Umgebung befindet. DIe Erstkommunikation wird mit einer Voreingestellten Endadresse initiiert und diese Erstadresse stellt ein Typenschild-Zertifikat aus (1), dass das Asset in seinen Eigenschaften beschreibt. Dieses Zertifikat ist mit dem privaten Schlüssel des Herstellers (hier Siemens) signiert und auf dem Asset gespeichert.

Auf Anfrage eines Aussenstehenden (hier DB) wird das Zertifkat übermittelt (2). Die DB erzeugt aus den Informationen auf dem Zertifikat ohne die Signatur eine Prüfsumme nach der im Zertifikat vorgegebenen Methode. Die Signatur wird durch den auf dem DID-Netzwerk verzeichneten öffentlichen Schlüssel dechiffriert und mit der Prüfsumme verglichen (3).
Diese Schritte werden durch die DID-Methodik und die Open-Source-Komponenten sicher abgewickelt. In diesem Projekt haben die Beteiligten den [aries-cloudagent-python](https://github.com/hyperledger/aries-cloudagent-python) verwendet und jede Partei für sich hat die Steuersoftware zur Nutzung implementiert.
