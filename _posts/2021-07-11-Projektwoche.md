---
layout: post
title:  RailChain-Projektwoche auf dem advanced TrainLab
date:   2021-07-11 15:32:14 -0100
---

In der RailChain-Projektwoche vom 06.07. bis zum 08.07.2021 haben wir den [advanced TrainLab](https://www.deutschebahn.com/de/Digitalisierung/technologie/advanced-TrainLab-das-schnellste-Labor-auf-Schienen-3953074) gebucht, um die Einbauten im Zug durch Fahrszenarien auf Herz- und Nieren zu testen.

Wir sind sehr froh darüber viele der Projektbeteiligten persönlich gesehen zu haben – natürlich alles unter strenger Einhaltung der Pandemievorschriften. Wir sind stolz auf die Ergebnisse, die ohne unsere Partner nicht möglich gewesen wären. Vielen Dank an dieser Stelle:

* an die Kollegen der Abteilung TVE 41 der [DB Systemtechnik](https://www.db-systemtechnik.de/dbst-de) für die Unterstützung beim Engineering für den Einbau der Komponenten und die Nutzung der Plattform FALCOS

* an die Firma [optiMEAS](https://www.optimeas.de/) für die Unterstützung bei der Entwicklung der Schnittstellen und der Betriebssysteme auf den Blockchain-Knoten

Bei der Gelegenheit konnten wir auch allen Projektteilnehmer:innen die Chance geben sich zum Projekt zu äussern:

<p>
<iframe  width="790" height="444"  src="https://www.youtube.com/embed/hTfnz0lnc5k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>
