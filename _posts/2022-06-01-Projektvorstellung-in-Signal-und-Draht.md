---
layout: post
title:  Projektvorstellung in der Fachzeitschrift "Signal & Draht"
date:   2022-06-01
---

Wir freuen uns, dass unsere Projektmitglieder
Prof. Dr. Jens Braband (Siemens),
Prof. Dr. Rüdiger Kapitza (TU Braunschweig),
Prof. Dr. Andreas Polze (Hasso-Plattner-Institut)
und Ingo Schwarzer (DB Systel)
mit den Artikel "RailChain – Anwendung von
Distributed-Ledger-Technologien im Bahnbetrieb" das Projekt ein
weiteres Mal öffentlich vorstellen durften.

Der Artikel erscheint in der etablierten Eisenbahn-Fachzeitschrift
["Signal & Draht"](https://www.eurailpress.de/abo/signal-draht.html) (Ausgabe Juni
2022) und [kann hier nachgelesen werden]({{ site.url }}/assets/2022-06_Signal-Draht_RailChain-Anwendung-von-Distributed-Ledger-Technologien-im-Bahnbetrieb_Braband-Kapitza-Polze-Schwarzer.pdf).
