---
layout: post
title:  Veröffentlichung der Ergebnisse zum Juridical Recording in der IEEE DSN 2022
date:   2022-07-01
---

Wir freuen uns, dass wir die Ergebnisse des Anwendungsfalls *Juridical Recording* in der wissenschaftlichen Veröffentlichung ["ZugChain: Blockchain-Based Juridical Data Recording in Railway Systems"](https://ieeexplore.ieee.org/abstract/document/9833566) auf der [52. IEEE/IFIP International Conference on Dependable Systems and Networks (DSN)](https://dsn2022.github.io/) vorstellen durften.

Die Veröffentlichung ist entstanden durch unsere Projektmitglieder
Signe Rüsch (TU Braunschweig),
Kai Bleeke (TU Braunschweig),
Ines Messadi (TU Braunschweig),
Stefan Schmidt (TU Braunschweig),
Andreas Krampf (Siemens Mobility),
Katharina Olze (Siemens Mobility),
Susanne Stahnke (Siemens AG),
Robert Schmid (Hasso-Plattner-Institut),
Lukas Pirl (Hasso-Plattner-Institut),
Roland Kittel (DB Systel),
Andreas Polze (Hasso-Plattner-Institut),
Marquart Franz (Siemens AG),
Matthias Müller (Siemens Mobility),
Leander Jehl (TU Braunschweig und University of Stavanger, Norway),
und Rüdiger Kapitza (TU Braunschweig).
