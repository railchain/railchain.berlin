---
layout: post
title:  Veröffentlichung in der Fachzeitschrift "Signal & Draht"
date:   2021-09-01
---

Wir freuen uns, dass unsere Projektmitglieder Prof. Dr. Jens Braband
(Siemens) und Hendrik Schäbe (TÜV Rheinland) einen Artikel zur
"Zuverlässigkeit von verteiltem Juridical Recording" veröffentlichen
konnten.

Der Artikel erscheint in der etablierten Eisenbahn-Fachzeitschrift
["Signal & Draht"](https://www.eurailpress.de/abo/signal-draht.html) (Ausgabe September
2021) und [kann hier nachgelesen werden]({{ site.url }}/assets/2021-09_Signal-Draht_Zuverlässigkeit-Juridical-Recording_Braband-Schaebe.pdf).
