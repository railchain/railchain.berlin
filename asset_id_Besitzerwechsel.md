---
layout: page
title: Asset ID – Besitzerwechsel
permalink: /asset_id_Besitzerwechsel/
hide_from_menu: true
---

Mit dem Prozess der Erstinitialisierung wird dem Asset durch den Hersteller ein Besitzer-Zertifikat ausgestellt. Die Signatur dieses Zertifikates wird beim Aufbau von Verbindungen mit dem Asset dazu benutzt, um die Zugriffsrechte des Kommunikationspartners zu definieren. Der Prozess "Besitzerwechsel" beispielsweise kann nur durch den jetzigen Besitzer ausgelöst werden. Der Ablauf ist dem folgenden Bild dargestellt:

<p style="text-align: center">
    <img src="../images/Railchain_AssetId_Besitzerwechsel.png" alt="Prozess des Besitzerwechsels" width="500px" />
</p>

Mit dem Beginn des Prozesses übermittelt der jetzige Besitzer (Siemens) die Identität des neuen Besitzers (DB). Dem Gerät wird somit "erlaubt" eine Anfrage zum Wechsel des neuen Besitzers nur von diesem anzunehmen.

Der neue Besitzer fragt den Besitzwechsel an und weist sich in seiner Identität aus, das Gerät verifiziert die Anfrage und bestätigt bei Übereinstimmung der Signaturen. Der neue Besitzer stellt ein neues Besitzerzertifikat aus, das Gerät nimmt dieses an und ersetzt das bisherige Besitzerzertifikat. Ab diesem Moment ist der Besitzerwechsel vollzogen und der vorhergehende Besitzer ist nicht mher in der Lage diesen Prozess auszulösen.

Für die Historie ist es möglich den Vorgang des Besitzerwechsels durch ein Historienzertifikat des alten oder des neuen Besitzers zu erhalten.

