---
layout: page
title: Prellblock Projekt am Hasso-Plattner-Institut
permalink: /prellblock/
hide_from_menu: true
---

Eine wesentliche Frage, die es zu behandeln gab, ist die des zu verwendenden Blockchain-Frameworks. Dazu sind am [Hasso-Plattner-Institut](https://hpi.de/) Referenzimplementierungen entworfen, implementiert und vermessen worden. Dabei sind die  Hyperledger-Frameworks *Indy*, *Sawtooth*, *Iroha* und *Besu* gegen eine Eigenimplementierung verglichen worden. Hyperledger *Iroha* und *Besu* sind nach ersten Tests aus der Bewertung genommen worden, weil bei den zu der Zeit der Untersuchungen bestehenden Projektzuständen der Frameworks eine weitere Betrachtung nicht sinnvoll schien.

Das folgende Diagramm stellt die durchschnittliche Latenz jeder Transaktion für die untersuchten Frameworks dar:
![Prellblock_Latenz](../images/Railchain_Prellblock_Latenz.png)
Sehr deutlich ist eine Transaktionslatenz von Hyperledger *Sawtooth* von über 1s zu erkennen, die unabhängig von der Nutzlastgrösse der einzelnen Transaktionen ist. Der Wert von 1s ist insofern kritisch, weil die Anforderung einer Datenaufzeichnung einer *Jridical Recording Unit* bei max. 1s liegt. Für den Anwendungsfall *Juridical Recording* scheidet das Framework von Hyperledger *Sawtooth* damit aus. Hyperledger *Indy* zeigt ein überraschend gutes Latenzverhalten, allerdings ist ab einer Nutzlast von 1kByte/Transaktion bereits ein Anstieg der Trasnaktionszeiten wahrzunehmen.

Das 2. Diagramm zeigt die Anzahl Transaktionen pro Sekunde abhängig von der Nutzlast pro Transaktion:
![Prellblock Durchsatz](../images/Railchain_Prellblock_Trans.png)
Hier zeigt Hyperledger *Indy* eine Grenze auf, die weit unter der Eigenimplementierung liegt. Hierbei muss auch angemerkt werden, dass die Testhardware ein vielfaches leistungsfähiger war, als die für den Einbau in einen Zug erhältlichen Komponenten.

Die Messwerte legen nahe, dass die in Frage kommenden Blockchain-Frameworks zu ressourcenhungrig sind, um auf einer möglichen Infrastruktur innerhalb eines Zuges funktionieren zu können.

Anbei ein Video zur Vorstellung der Projektergebnisse des Bachelor Projektes
[HPI Prellblock Bachelor Projekt](https://vimeo.com/454303212/6175d23f4d)

Besonderer Dank an dieser Stelle geht an die HPI-Mitarbeiter des [Fachgebietes Betriebssysteme und Middleware](https://osm.hpi.de/):

- Prof. Dr. Andreas Polze,
- die wissenschaftlichen Mitarbeiter Lukas Pirl und Robert Schmid,
- und vor allem an die beteiligten Studierenden an der Prellblock-Studie:
  Benedikt Schenkel, Martin Michaelis, Felix Gohla und Malte Andersch
