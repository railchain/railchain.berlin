---
layout: page
title: Legitimierte Wartung durch einen Partner
permalink: /asset_id_Partnerwartung/
hide_from_menu: true
---

Der Ablauf zur Legitimation eines Partners zur Durchführung einer Wartungsoperation an einem Asset setzt das Vorhandensein einer Verbindung zwischen dem Besitzer und dem Partner eines Assets vorraus.

Der Ablauf der Authorisierung und der Legitimation des Wartungspartners ist im  folgenden Bild dargestellt:
![Wartung, Prüfen der Berechtigung ](../images/Railchain_AssetId_Wartung_1.png)

Der etablierte Verbindungskanal zwischen den Parteien wird genutzt, um dem Wartungspartner (hier Spherity) ein entsprechendes Zertifikat auszustellen (1). Die Informationen des Zertifikates können feingranular ausgestaltet werden, um die Baureihe oder die Wartungsvorgänge selbst zielgerichtet einzuschränken.

Die weiteren Schritte beschreiben den Ablauf der Legitimation des Wartungspartners bei der Anfrage an das Asset. Der Wartungspartner erstellt eine Verbindung mit dem Asset mit der Bitte eine Wartung durchführen zu dürfen (2). Das Asset erwidert diese Anfrage mit der Nachfrage nach einer Legitimation für die Berechtigung der Durchführung (3). Der Wartungspartner antwortet darauf mit dem Wartungszertifikat des Besitzers (4).

Das Asset prüft das Wartungszertifikat auf Integrität und ob die Art der Wartung auch legitimiert ist. Sollten diese kriterien nicht eingehalten werden, so wird die Verbindung seitens des Assets beendet. Im Falle einer Übereinstimmung der Prüfkriterien wird die Wartung vom Asset zugelassen. Im folgenden Bild sind die Prozesschritte dargestellt, die bei einer Wartung umgesetzt werden:

<p style="text-align: center">
    <img src="../images/Railchain_AssetId_Wartung_2.png" alt="Ausstellen eines Wartungszertifikates" width="400px" />
</p>

Die Wartung wird im interaktiven Dialog über den sicheren Verbindungskanal durchgeführt (1). Nach erfolgreicher Durchführung stellt der Wartungspartner ein Zertifikat aus, dass die erfolgreiche Wartung dokumentiert (2).
