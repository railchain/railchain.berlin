---
layout: page
title: Data Logger
permalink: /data_logger/
hide_from_menu: true
---

Der zweite Anwendungsfall ist die Implementierung eines Daten-Loggers auf Blockchain-Basis ohne Echtzeitanforderungen.
Hierbei sollen Daten wie z. B. Verspätungsinformationen oder Sensormesswerte und Diagnosemeldungen erfasst und zuverlässig und nicht manipulierbar auf einer dezentralen Blockchain abgespeichert werden.
Dabei wird zusätzlich sichergestellt, dass die einzelnen Informationen und Komponenten keine unterschiedlichen, abweichenden Zustände melden (z. B. Nachrichten in unterschiedlicher Reihenfolge loggen), sondern Daten erst dann in der Blockchain hinterlegt werden, wenn ihre Ordnung und Echtheit sichergestellt werden kann.

Eine wesentliche Frage, die es zu behandeln gab, ist die des zu verwendenden Blockchain-Frameworks. Das Fachgebiet Betriebssysteme und Middleware am Hasso-Plattner-Institut hat dazu Untersuchungen durchgeführt, deren [Ergebnisse](../prellblock/)  nahelegen, dass die verfügbaren Blockchain-Frameworks zu ressourcenhungrig für eine Anwendung innerhalb eines Zuges sind.

Die Alternative zu den schwergewichtigen Blockchain-Frameworks ist eine Eigenimplementierung. Diese wurde am Institut für Betriebssysteme und Rechnerverbund der TU Braunschweig umgesetzt und zusammen mit den Spezialisten der Siemens Mobility GmbH ist dabei die [Referenzimplementierung](../referenzimp/), die wichtige Erkenntnisse und Grundlagen für das Systemdesign der Komponenten für den Einbau in den *advanced TrainLab* geliefert hat.

Der [Einbau in den Testzug](/atl_einbau/) *advanced TrainLab* und die Testfahrten, die in der [Projektwoche](/2021/07/11/Projektwoche) Anfang Juli 2021 durchgeführt werden konnten, stellen einen Höhepunkt des gesamten Projektes dar. Die hier sehr verkürzt beschriebene Historie gibt nur grob wieder wie jedes einzelne Teilergebnis zu dem bisher sehr erfolgreichen Projektablaufes führte.
