---
layout: page
title: Einbauten in den advanced TrainLab
permalink: /atl_einbau/
hide_from_menu: true
---

Das RailChain-Projekt hatte die Demonstration in einem realen Umfeld von Anfang an im Konzept vorgesehen. Ein wesentliche Hürde für den Einbau ist die Erhaltung der Betriebszulassung des Zuges. Der MVB-Bus, die wesentliche Informationsquelle für die Aufzeichnung des Datenstroms, ist auch eine kritische Komponente. Die Konfiguration des Busses ist Teil des Systemdesigns des Triebfahrzeuges und kann nicht angetastet werden ohne eine neue Genehmigung für den Betrieb zu beantragen. Dieser aufwändige Prozess musste unbedingt verhindert werden, weil durch eine Neuaufnahme viele Aspekte neu verhandelt werden müssten, die noch unter Bestandssschutz fallen.

Der MVB ist glücklicherweise ein Buskonzept, dass lesenden Zugriff über die Nutzung einer Schnittstelle zulässt. Die Nachweispflicht beschränkt sich auf die reine Lesbarkeit ohne Antwortfähigkeiten, um den Busablauf nicht zu stören. Für das Engineering des Einbaus und die Vorarbeit der Genehmigung hat sich sehr früh im Projektverlauf eine Kooperation mit der DB Systemtechnik ergeben. Ihr Projekt *AssetData2Value* passte sehr gut mit dem Vorhaben von RailChain zusammen und Sie haben auch den Kontakt zur optiMEAS GmbH ermöglicht, die mit uns die Schnittstelle zwischen der Telematik-Einheit der DB Systemtechnik und unseren Blockchain-Knoten entwickelt hat. Beide Partner haben sich weit über das normal erwartbare Maß hinaus für das Projekt eingesetzt.

Das folgende Bild zeigt das Konzept für die Einbauten:
![RailChain-Konzept-Einbauten in den advanced TrainLab](../images/Railchain_Einbau_Konzept.png)

Nach viel Vorbereitung zur Beschaffung und Finanzierung sind die Komponenten schliesslich eingebaut worden:
<p style="text-align: center">
    <img src="../images/Railchain_Einbau_1.png" alt="RailChain aTL-Einbau 1" width="500px" />
</p>
<p style="text-align: center">
    <img src="../images/Railchain_Einbau_BC_Knoten.png" alt="RailChain aTL-Einbau BC-Knoten" width="500px" />
</p>

Eine JRU des Testzuges zeichnet ca. 31 Signale des MVB auf. Modernere Systeme werden für bis zu 100 SIgnale konfiguriert. Das Testsystem unseres Projektes zeichnet ein Set aus 400 Signalen auf, es sind aber mehr als doppelt soviele noch möglich.

Das folgende Bild zeigt das Dashboard der Systemtechnik Falcos, das im Projektverlauf entstanden ist, um die ZUgdaten in Echtzeit darstellen zu können.

![Dashboard RailChain im advanced TrainLab](../images/Railchain_Falcos.png)

Besonderer Dank an dieser Stelle geht an:

Die Mitarbeiter des [advanced TrainLab](https://www.deutschebahn.com/de/Digitalisierung/technologie/advanced-TrainLab-das-schnellste-Labor-auf-Schienen-3953074): Marc Simon und Michael Brandecker

Die Mitarbeiter der [DB Systemtechnik](https://www.db-systemtechnik.de/dbst-de):
Jan Eichhorn, Andreas Isbarn, Jörg Diestel, Jörn Raguse und Jens Knittel

Die Mitarbeiter der [optiMEAS GmbH](https://www.optimeas.de/):
Burkhard Schranz, Michael Baron, Matthias Klein und Simon Gilz
